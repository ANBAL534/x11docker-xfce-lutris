# x11docker-xfce-lutris
XFCE desktop in Docker image. Based on Arch Linux with Mesa and Vulkan-Intel + Vulkan-Radeon for game acceleration, including lib32 versions of needed libraries.
 - DRI_PRIME capable if set up beforehand in host.
 - Run Lutris in Docker.
 - Use [x11docker](https://github.com/mviereck/x11docker) to run GUI applications and desktop environments in docker images. 

# Build the image
Use: `docker build -t x11docker-xfce .`
To build the image to be used with x11docker

# Command examples: 
As this image is intended for gaming, you want to sacrifice isolation for performance, also, you most probably want GPU acceleration
 - Launch Lutris: `x11docker --backend=docker --home=/absolute/path/to/an/empty/folder --pulseaudio --network --hostdisplay --gpu xfce-lutris:latest lutris`

Note: There is no interaction with the local browser, so you will need to use the game search functione inside lutris to select the game installers.

# Options:
 - Persistent home folder stored on host with   `--home`
 - Shared host file or folder with              `--share PATH`
 - Hardware acceleration with option            `--gpu`
 - Clipboard sharing with option                `--clipboard`
 - ALSA sound support with option               `--alsa`
 - Pulseaudio sound support with option         `--pulseaudio`
 - Language locale settings with                `--lang [=$LANG]`

Look at `x11docker --help` for further options.

# Extend base image
To add your desired applications, create your own Dockerfile with this image as a base. Example:
```
FROM xfce-lutris:latest
RUN yay -Syuu
RUN yay -S firefox
```

# TODOs
 - Add support for nvidia (I don't have a Nvidia GPU so I can't really test this functionality, feel free to send a PR for this.)
