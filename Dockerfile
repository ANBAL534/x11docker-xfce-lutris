FROM archlinux:latest

# Install requirments to run graphical programs
RUN pacman -Syuu --noconfirm && pacman -S --noconfirm xfce4 xfce4-goodies curl dbus docker perl pulseaudio python2 python3 xorg-setxkbmap tar unzip wmctrl wget xorg-xauth xclip xdg-utils xdotool xorg-xdpyinfo xorg-xhost xorg-xkbcomp xorg-xinit xorg-server-xnest xorg-server xorg-xrandr xsel xorg-xwininfo mesa-demos glibc libpulse systemd mesa-utils git base-devel sudo vulkan-mesa-layers vulkan-tools vulkan-extra-tools vulkan-radeon vulkan-intel --needed

# Enable multilib
RUN printf "[multilib]\nInclude = /etc/pacman.d/mirrorlist" >> /etc/pacman.conf

# Install 32bit versions of requirements
RUN pacman -Syuu --noconfirm wine-staging innoextract gamemode lib32-gamemode lib32-vkd3d lib32-vulkan-icd-loader vkd3d lib32-vulkan-mesa-layers xorg-xgamma lib32-mesa lib32-vulkan-radeon lib32-vulkan-intel

# Install yay
RUN mkdir /build && chmod 777 /build
WORKDIR /build

RUN useradd --no-create-home --shell=/bin/false build && usermod -L build
RUN echo "build ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
RUN echo "root ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

USER build
RUN git clone https://aur.archlinux.org/yay-bin.git && cd yay-bin && makepkg -s yay-bin
USER root
WORKDIR /build/yay-bin
RUN pacman -U --noconfirm *.pkg.tar.zst

# Install Lutris
RUN yay -Sy && yay -S --noconfirm lutris

# Configure XFCE
RUN Configfile="~/.config/xfce4/xfconf/xfce-perchannel-xml/xfwm4.xml" && \
echo "#! /bin/bash\n\
xdpyinfo | grep -q -i COMPOSITE || {\n\
  echo 'x11docker/xfce: X extension COMPOSITE is missing.\n\
Window manager compositing will not work.\n\
If you run x11docker with option --nxagent,\n\
you might want to add option --composite.' >&2\n\
  [ -e $Configfile ] || {\n\
    mkdir -p $(dirname $Configfile)\n\
    echo '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\
<channel name=\"xfwm4\" version=\"1.0\">\n\
\n\
  <property name=\"general\" type=\"empty\">\n\
    <property name=\"use_compositing\" type=\"bool\" value=\"false\"/>\n\
  </property>\n\
</channel>\n\
' > $Configfile\n\
  }\n\
}\n\
startxfce4\n\
" > /usr/local/bin/start && \
chmod +x /usr/local/bin/start

CMD start
